db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: `$supplier`, totalStocks: {$sum: `$stocks`}}}
		]
	)