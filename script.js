//mini act

db.fruits.insertMany(
		[
			{
				name: `Apple`,
				supplier: `Red Farms Inc.`,
				stocks: 20,
				price: 40,
				onSale: true
			},
			{
				name: `Banana`,
				supplier: `Yellow Farms`,
				stocks: 15,
				price: 20,
				onSale: true
			},
			{
				name: `Kiwi`,
				supplier: `Green Farming and Canning`,
				stocks: 25,
				price: 50,
				onSale: true
			},
			{
				name: `Mango`,
				supplier: `Yellow Farms`,
				stocks: 10,
				price: 60,
				onSale: true
			},
			{
				name: `Dragon Fruit`,
				supplier: `Red Farms Inc.`,
				stocks: 10,
				price: 60,
				onSale: true
			}
		]
	)

//aggregation pipelines - done in 2-3 steps. each step is called pipeline
//$match is used to pass documents which satisfies or matches condition
//$group - allows to group documents together and create an analysis out of it

db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: `$supplier`, totalStocks: {$sum: `$stocks`}}}
		]
	)

db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: `totalOnSaleStocks`, totalStocks: {$sum: `$stocks`}}}
		]
	)

db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: null, totalStocks: {$sum: `$stocks`}}}
		]
	)

db.fruits.aggregate(
		[
			{$match: {supplier: `Yellow Farms`}},
			{$group: {_id: "Yellow Farms Stock", totalStocks: {$sum: `$stocks`}}}
		]
	)

//mini act

db.fruits.aggregate(
		[
			{$match: {supplier: `Red Farms Inc.`}},
			{$group: {_id: `Red Farms Inc.`, totalStocks: {$sum: `$stocks`}}}
		]
	)

db.fruits.aggregate(
		[
			{$match: {supplier: {$regex: `red`, $options: `$i`}}},
			{$group: {_id: `Red Farms Inc.`, totalStocks: {$sum: `$stocks`}}}
		]
	)

//avg - is an operator mostly used for the $group stage
//gets the avg of the numerical values of the indicated field field

db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: `$supplier`, avgStock: {$avg: `$stocks`}}}
		]
	)

db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: `$supplier`, avgPrice: {$avg: `$price`}}}
		]
	)

//max - gets the highest value fr the selected criteria

db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: `$supplier`, max_Price: {$max: `$price`}}}
		]
	)

db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: `Supplier`, max_stock: {$max: `$stocks`}}}
		]
	)

//$min - returns the lowest value

db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$group: {_id: `Supplier`, max_stock: {$min: `$stocks`}}}
		]
	)

//mini act

db.fruits.aggregate([
		{$match: {price: {$lt: 50}}},
		{$group: {_id: `Lowest Stock`, minimum_stock: {$min: `$stocks`}}}
	])

//count - total number of record

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$count: `itemsOnSale`}
	])

db.fruits.aggregate([
		{$match: {price: {$lt: 50}}},
		{$count: `itemsOnSale`}
	])

db.fruits.aggregate([
		{$match: {price: {$gt: 40}}},
		{$count: `Total Number`}
	])

db.fruits.aggregate([
		{$match: {stocks: {$lt: 20}}},
		{$count: `Total Number`}
	])

//out - will save the result in a new collection
//note - this will overwrite the collection if it already exists

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: `$supplier`, totalStocks: {$max: `$stocks`}}},
		{$out: `stocksSupplier`}
	])